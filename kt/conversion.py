#!/usr/bin/python
# -*- coding:utf-8 -*-
'''
卡丁车轮胎规格换算
10X3.60-5是轮胎的规格
具体是轮胎外直径10in，约254mm
轮胎断面宽3.6in,约92mm
轮毂直径5in，约127mm
1in(英寸)=25.4mm
50公里时速=50000米/3600秒=13.89米/秒
转数=13.89米每秒/周长=
'''
import os
while True:
    doscls = os.system("cls")
    inch = 25.4
    # 输入直径（英寸）1in(英寸)=25.4mm
    Diameter = float(input("请输入直径（单位：英寸）：")) * inch
    # 计算周长
    Perimeter = Diameter * 3.14159265
    # 计算时速
    Speed = 13.89 / Perimeter*1000
    # 输入轮胎断面（英寸）
    Tire_Section = float(input("请输入轮胎断面（单位：英寸）：")) * inch
    # 输入轮毂直径（英寸）
    Wheel_Diamte = float(input("请输入轮毂直径（单位：英寸）：")) * inch
    # 开始打印信息
    print("-=-=-=-=-=-=-=-=-=-=-")
    print("轮胎直径=%.2f毫米" % Diameter)
    print("轮胎周长=%.2f毫米" % Perimeter)
    print("轮胎断面宽=%.2f毫米" % Tire_Section)
    print("轮毂直径=%.2f毫米" % Wheel_Diamte)
    print("50公里时速=%.2f转/秒" % Speed)
    print("-=-=-=-=-=-=-=-=-=-=-")
    reload = input("---点击回车重新计算---")
