# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import cv2
import sys


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1222, 866)
        self.formLayoutWidget = QtWidgets.QWidget(Form)
        self.formLayoutWidget.setGeometry(QtCore.QRect(700, 120, 481, 121))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout_3 = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout_3.setContentsMargins(0, 0, 0, 0)
        self.formLayout_3.setObjectName("formLayout_3")
        self.iplabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.iplabel.setObjectName("iplabel")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.iplabel)
        self.iplineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.iplineEdit.setObjectName("iplineEdit")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.iplineEdit)
        self.duankoulabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.duankoulabel.setObjectName("duankoulabel")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.duankoulabel)
        self.duankoulineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.duankoulineEdit.setObjectName("duankoulineEdit")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.duankoulineEdit)
        self.quedingButton = QtWidgets.QPushButton(self.formLayoutWidget)
        self.quedingButton.setObjectName("quedingButton")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.quedingButton)
        self.shipinlabel = QtWidgets.QLabel(Form)
        self.shipinlabel.setGeometry(QtCore.QRect(100, 50, 521, 441))
        self.shipinlabel.setObjectName("shipinlabel")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.iplabel.setText(_translate("Form", "ip地址："))
        self.duankoulabel.setText(_translate("Form", "端口号："))
        self.quedingButton.setText(_translate("Form", "确定"))
        self.shipinlabel.setText(_translate("Form", "TextLabel"))

class MainWindow(QtWidgets.QMainWindow, Ui_Form):
    def __init__(self):
        super(MainWindow,self).__init__()

        self.setupUi(self)
        self.initUI()

    def initUI(self):

        self.quedingButton.clicked.connect(self.start)
    def start(self):

        s1 = self.iplineEdit.text()
        s2 = self.duankoulineEdit.text()
        s3 = "http://"+s1+":"+s2+"/?action=stream"

        self.timerCamera = QtCore.QTimer(self)
        self.cap = cv2.VideoCapture(s3)
        self.timerCamera.timeout.connect(self.showPic)
        self.timerCamera.start(10)

    def showPic(self):
        success, frame = self.cap.read()

        #图像水平垂直翻转
        #frame = cv2.flip(frame, -1)
        if success:
            #转换图像空间
            show = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            showImage = QtGui.QImage(show.data, show.shape[1], show.shape[0], QtGui.QImage.Format_RGB888)
            self.shipinlabel.setPixmap(QtGui.QPixmap.fromImage(showImage))
            self.timerCamera.start(10)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())



