#!/usr/bin/python
# -*- coding:utf-8 -*-
import os
doscls = os.system("cls")
i = 1
while i <= 19:
    j = 1
    while j <= i:
        print("%d*%d=%d\t" % (i, j, i*j), end=" ")
        j += 1
    print("")
    i += 1
