#!/usr/bin/python
# -*- coding:utf-8 -*-
import os
i = os.system("cls")
xmhi = float(input('请输入身高(米):'))
xmtz = float(input('请输入体重(公斤):'))
bmi = round((xmtz/(xmhi*xmhi)), 2)  # 取小数点后两位
if bmi > 32:
    print('BMI指数为', bmi, '严重肥胖')
elif bmi >= 28:
    print('BMI指数为', bmi, '肥胖')
elif bmi >= 25:
    print('BMI指数为', bmi, '过重')
elif bmi >= 18.5:
    print('BMI指数为', bmi, '正常')
elif bmi < 18.5:
    print('BMI指数为', bmi, '过轻')
else:
    pass
