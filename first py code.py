#!/usr/bin/python
# -*- coding:utf-8 -*-
# 这个可以在DOS执行前清除DOS窗口下的屏幕
import time
import os  # 导入系统模块
i = os.system("cls")

# 延时不是像Arduino那样,直接delay(),先导入time模块,再用time.sleep()延时
print('越过学习细节,直接找自己要用的')
time.sleep(1)
print('倒计时: 5')
time.sleep(1)
print('倒计时: 4')
time.sleep(1)
print('倒计时: 3')
time.sleep(1)
print('倒计时: 2')
time.sleep(1)
print('倒计时: 1')
time.sleep(1)
print('这是我的一个延时测试结束')
time.sleep(1)
print('程序正在退出并清空DOS屏幕')
time.sleep(1)
i = os.system("cls")  # 退出程序后执行DOS清屏命令我想测试一下码云的上传功能
