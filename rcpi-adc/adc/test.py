#!/usr/bin/python
# -*- coding:utf-8 -*-

import os
import sys
import time
import ADS1256
import RPi.GPIO as GPIO
pin=12
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin,GPIO.OUT)
os.system("clear")
p=GPIO.PWM(pin,400)
p.start(0)
try:
    ADC = ADS1256.ADS1256()
    ADC.ADS1256_init()
    while(1):
        ADC_Value = ADC.ADS1256_GetAll()
        print ("0 ADC = %lf"%(ADC_Value[0]*5.0/0x7fffff),)
        print ("1 ADC = %lf"%(ADC_Value[1]*5.0/0x7fffff),)
        print ("2 ADC = %lf"%(ADC_Value[2]*5.0/0x7fffff),)
        print ("3 ADC = %lf"%(ADC_Value[3]*5.0/0x7fffff),end='  输出值为')
        # 200*11.6 7-49  400*22.3 14-99
        # 100*5.6 4-18 
        dc=int((ADC_Value[3]*5.0/0x7fffff)*22.3)
        if (dc<58) and (dc>52):
            dc=55
        elif (dc>=98):
            dc=98
        elif (dc<=14):
            dc=14
        print (dc)
        p.ChangeDutyCycle(dc)
        print ("4 ADC = %lf"%(ADC_Value[4]*5.0/0x7fffff),)
        print ("5 ADC = %lf"%(ADC_Value[5]*5.0/0x7fffff),)
        print ("6 ADC = %lf"%(ADC_Value[6]*5.0/0x7fffff),)
        print ("7 ADC = %lf"%(ADC_Value[7]*5.0/0x7fffff))
        print ("以上为ADC实时数据")
        print ("\33[12A")
#0x7fffff为16进制代码,对应数值是-8388609
except :
    server.close()
    p.stop()
    GPIO.cleanup()
    print ("\r\nProgram end     ")
    exit()
