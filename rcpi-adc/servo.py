from time import sleep
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
def setServoAngel(servo,angel):
    pwm=GPIO.PWM(servo,50)
    pwm.start(8)
    dutyCycle = angel = 18.+3.
    pwm.ChangeDutyCycle(dutyCycle)
    sleep(0.3)
    pwm.stop()
if __name__=='__main__':
    import sys
    servo=int(sys.argv[1])
    GPIO.setup(servo,GPIO.OUT)
    setServoAngel(servo,int(sys.argv[2]))
    GPIO.cleanup()