import socket
import os
import sys
import time
import ADS1256
pin = 12
os.system("clear")
BUFSIZE = 1024
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
try:
    ADC = ADS1256.ADS1256()
    ADC.ADS1256_init()
    while True:
        ADC_Value = ADC.ADS1256_GetAll()
        print("0 ADC = %lf" % (ADC_Value[0]*5.0/0x7fffff),)
        print("1 ADC = %lf" % (ADC_Value[1]*5.0/0x7fffff),)
        print("2 ADC = %lf" % (ADC_Value[2]*5.0/0x7fffff),)
        print("3 ADC = %lf" % (ADC_Value[3]*5.0/0x7fffff),)
        print("4 ADC = %lf" % (ADC_Value[4]*5.0/0x7fffff),)
        print("5 ADC = %lf" % (ADC_Value[5]*5.0/0x7fffff),)
        print("6 ADC = %lf" % (ADC_Value[6]*5.0/0x7fffff),)
        print("7 ADC = %lf" % (ADC_Value[7]*5.0/0x7fffff),)

        sp = int((ADC_Value[2]*5.0/0x7fffff)*22)
        dc = int((ADC_Value[3]*5.0/0x7fffff)*22)
        # 这里的sp和dc都要作限幅,限制在0-99范围内
        if (dc >= 98):
            dc = 98
        elif (dc <= 1):
            dc = 1
        if (sp >= 98):
            sp = 98
        elif (sp <= 1):
            sp = 1
        print("%02d %02d" % (dc, sp))
        # while True:
        # msg数据输出要限制在每2位为一个通道
        msg = "%02d%02d".strip() % (dc, sp)
        ip_port = ("192.168.1.7", 9999)
        client.sendto(msg.encode('utf-8'), ip_port)
        #data,server_addr = client.revbfrom(BUFSIZE)
        # print("客户端返回",data,server_addr)
        print("客户端摇杆输入信息")
        print("\33[12A")
except:
    client.close()
    exit()
