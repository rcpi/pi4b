# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'a:\mypy\car\control\video\video.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(1024, 540)
        mainWindow.setMinimumSize(QtCore.QSize(1024, 540))
        mainWindow.setMaximumSize(QtCore.QSize(1024, 540))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        mainWindow.setFont(font)
        mainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(280, 500, 421, 27))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.info_lable = QtWidgets.QLabel(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setKerning(False)
        self.info_lable.setFont(font)
        self.info_lable.setObjectName("info_lable")
        self.horizontalLayout.addWidget(self.info_lable)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.lineEdit_2.setInputMask("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout.addWidget(self.lineEdit_2)
        self.open_pushButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.open_pushButton.setObjectName("open_pushButton")
        self.horizontalLayout.addWidget(self.open_pushButton)
        self.video_label = QtWidgets.QLabel(self.centralwidget)
        self.video_label.setGeometry(QtCore.QRect(180, 0, 640, 480))
        self.video_label.setText("")
        self.video_label.setObjectName("video_label")
        self.close_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.close_pushButton.setGeometry(QtCore.QRect(920, 500, 75, 23))
        self.close_pushButton.setObjectName("close_pushButton")
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(7, 20, 158, 31))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.open_wl_pushButton = QtWidgets.QPushButton(self.horizontalLayoutWidget_2)
        self.open_wl_pushButton.setObjectName("open_wl_pushButton")
        self.horizontalLayout_2.addWidget(self.open_wl_pushButton)
        self.close_wl_pushButton = QtWidgets.QPushButton(self.horizontalLayoutWidget_2)
        self.close_wl_pushButton.setObjectName("close_wl_pushButton")
        self.horizontalLayout_2.addWidget(self.close_wl_pushButton)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(59, 0, 51, 23))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label.setFont(font)
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setAutoFillBackground(False)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setScaledContents(True)
        self.label.setIndent(-1)
        self.label.setObjectName("label")
        mainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(mainWindow)
        self.close_pushButton.clicked.connect(mainWindow.close)
        self.lineEdit_2.returnPressed.connect(self.open_pushButton.click)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(_translate("mainWindow", "Rcpi.top 远程控制系统 Alpha 1.0 build by RCPI"))
        self.info_lable.setText(_translate("mainWindow", "请输入IP:Port"))
        self.lineEdit_2.setText(_translate("mainWindow", "192.168.1.7:8080"))
        self.lineEdit_2.setPlaceholderText(_translate("mainWindow", "192.168.1.7:8080"))
        self.open_pushButton.setText(_translate("mainWindow", "打  开"))
        self.close_pushButton.setText(_translate("mainWindow", "关闭窗口"))
        self.open_wl_pushButton.setText(_translate("mainWindow", "开"))
        self.close_wl_pushButton.setText(_translate("mainWindow", "关"))
        self.label.setText(_translate("mainWindow", "警示灯"))
