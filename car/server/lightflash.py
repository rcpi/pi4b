import time
#import Adafruit_NeoPixel as rpi_ws281x
from rpi_ws281x import Adafruit_NeoPixel, Color
import RPi.GPIO as GPIO
LED_COUNT = 8
LED_PIN = 21
LED_BRIGHTNESS = 255

LED_FREQ_HZ = 800000
LED_DMA = 10
LED_INVERT = False
SleepTime = 0.05
FlashNum = 8
i = 0
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ,
                          LED_DMA, LED_INVERT, LED_BRIGHTNESS)
strip.begin()
# 这里是红蓝爆闪，红闪8次，每次间隔0.05秒，停0.8秒，蓝闪8次，每次间隔0.05秒，停0.8秒。然后循环
while True:
    for a in range(0, FlashNum):
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, Color(255, 0, 0))
            strip.show()
        time.sleep(SleepTime)
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, Color(0, 0, 0))
            strip.show()
        time.sleep(SleepTime)
        a = a+1
        print("红闪%d次,间隔0.05秒!" % a)
    print("这里休息0.8秒")
    time.sleep(0.8)
    for b in range(0, FlashNum):
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, Color(0, 0, 255))
            strip.show()
        time.sleep(SleepTime)
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, Color(0, 0, 0))
            strip.show()
        time.sleep(SleepTime)
        b = b+1
        print("蓝闪%d次,间隔0.05秒!" % b)
    print("这里休息0.8秒")
    time.sleep(0.8)
    light_info = 0


# 下边是循环的红绿蓝+1
#     for i in range(0,strip.numPixels()):
#         strip.setPixelColor(i,Color(255,0,0))
#         strip.show()
#         time.sleep(0.1)
#     for i in range(0,strip.numPixels()):
#         strip.setPixelColor(i,Color(0,0,0))
#         strip.show()
#         time.sleep(0.1)
#     for i in range(0,strip.numPixels()):
#         strip.setPixelColor(i,Color(0,255,0))
#         strip.show()
#         time.sleep(0.1)
#     for i in range(0,strip.numPixels()):
#         strip.setPixelColor(i,Color(0,0,0))
#         strip.show()
#         time.sleep(0.1)
#     for i in range(0,strip.numPixels()):
#         strip.setPixelColor(i,Color(0,0,255))
#         strip.show()
#         time.sleep(0.1)
#     for i in range(0,strip.numPixels()):
#         strip.setPixelColor(i,Color(0,0,0))
#         strip.show()
#         time.sleep(0.1)
