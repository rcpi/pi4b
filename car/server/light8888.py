#!/usr/bin/python
# -*- coding:utf-8 -*-
# 这里作一个统一的接口，除实体输入设备外，其它屏幕输入信息都接入这里，接收到信息后，自动启动指定程序。
import os
import sys
import time
import socket
BUFSIZE = 1024
ip_port = ("0.0.0.0", 8888)
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP协议
server.bind(ip_port)

while True:
    data, client_addr = server.recvfrom(BUFSIZE)
    server.sendto(data.upper(), client_addr)
    print("服务器收到数据", data)
    data1 = int(data[0:1])
    data2 = int(data[1:2])
    print("data1", data1)
    print("data2", data2)
    if data1 == 0:
        # 这个是红蓝爆闪的启动项
        os.system("nohup python ./lightflash.py >/dev/null 2>&1 &")
        # 这个是流水的,可以用作转向或游侠灯启动
        #os.system("nohup python ./lightfolw.py >/dev/null 2>&1 &")
    if data1 == 1:
        # 这个是红蓝爆闪的关闭项
        os.system(
            "ps -ef | grep lightflash.py | grep -v grep | awk '{print $2}' | xargs kill -9")
        # 这个是流水的,可以用作转向或游侠灯关闭
        #os.system("ps -ef | grep lightfolw.py | grep -v grep | awk '{print $2}' | xargs kill -9")
