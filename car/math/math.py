import math
import os
os.system("cls")
# 输入必须参数，ABCD四个点的距离及AC/BD距离
ab = float(eval(input('请输入AB长：')))
bc = float(eval(input('请输入BC长：')))
cd = float(eval(input('请输入CD长：')))
da = float(eval(input('请输入DA长：')))
ac = float(eval(input('请输入AC长：')))
bd = float(eval(input('请输入BD长：')))
ax = float(eval(input('请输入AX长：')))
bx = float(eval(input('请输入BX长：')))

# 第一步，使用点BDA参数计算A角角度，使用ABC参数计算B角角度，使用BCD参数计算C角角度，使用CDA参数计算D角角度
# 1，计算A角角度
print('----------ABCD四点角度计算*开始----------')
horn_a = round(math.acos((ab * ab + da * da - bd * bd) /
                         (2 * ab * da)) * 180 / math.pi, 2)
horn_b = round(math.acos((ab * ab + bc * bc - ac * ac) /
                         (2 * ab * bc)) * 180 / math.pi, 2)
horn_c = round(math.acos((bc * bc + cd * cd - bd * bd) /
                         (2 * bc * cd)) * 180 / math.pi, 2)
horn_d = round(math.acos((cd * cd + da * da - ac * ac) /
                         (2 * cd * da)) * 180 / math.pi, 2)

print('A角为', horn_a, "度")
print('B角为', horn_b, "度")
print('C角为', horn_c, "度")
print('D角为', horn_d, "度")

print('----------ABCD四点角度计算*结束----------')
print()
print()
print('----------horn_ab内角角度及各边长度计算*开始----------')
horn_ab_a = round(math.acos((ab * ab + ax * ax - bx * bx) /
                            (2 * ab * ax)) * 180 / math.pi, 2)
horn_ab_b = round(math.acos((ab * ab + bx * bx - ax * ax) /
                            (2 * ab * bx)) * 180 / math.pi, 2)
horn_ab_x = round(math.acos((ax * ax + bx * bx - ab * ab) /
                            (2 * ax * bx)) * 180 / math.pi, 2)

print('边AB的长度为:', ab)
print('边AX的长度为:', ax)
print('边BX的长度为:', bx)
print("horn_ab_a角的度数为：", horn_ab_a, "度")
print("horn_ab_b角的度数为：", horn_ab_b, "度")
print("horn_ab_x角的度数为：", horn_ab_x, "度")
print('----------horn_ab内角角度及各边长度计算*结束----------')
print()
print()
print('----------horn_bc内角角度及各边长度计算*开始----------')
horn_bc_b = horn_b - horn_ab_b
cx = round(math.sqrt(bc * bc + bx * bx - 2 * bc *
                     bx * math.cos(math.radians(horn_bc_b))), 2)
horn_bc_c = round(math.acos((bc * bc + cx * cx - bx * bx) /
                            (2 * bc * cx)) * 180 / math.pi, 2)
horn_bc_x = 180 - horn_bc_b - horn_bc_c
print('边BC的长度为:', bc)
print('边BX的长度为:', bx)
print('边CX的长度为:', cx)
print("horn_bc_b角的度数为:", horn_bc_b, "度")
print('horn_bc_c角的度数为:', horn_bc_c, "度")
print('horn_bc_x角的度数为:', horn_bc_x, "度")
print('----------horn_bc内角角度及各边长度计算*结束----------')
print()
print()
print('----------horn_cd内角角度及各边长度计算*开始----------')
horn_cd_c = horn_c - horn_bc_c
dx = round(math.sqrt(cd * cd + cx * cx - 2 * cd *
                     cx * math.cos(math.radians(horn_cd_c))), 2)
horn_cd_d = round(math.acos((cd * cd + dx * dx - cx * cx) /
                            (2 * cd * dx)) * 180 / math.pi, 2)
horn_cd_x = 180 - horn_cd_c - horn_cd_d

print('边CD的长度为:', bc)
print('边CX的长度为:', cx)
print('边DX的长度为:', dx)
print('horn_cd_c角的度数为:', horn_cd_c, '度')
print('horn_cd_d角的度数为:', horn_cd_d, '度')
print('horn)cd_x角的度数为:', horn_cd_x, '度')

print('----------horn_cd内角角度及各边长度计算*结束----------')
print()
print()
print('----------horn_da内角角度及各边长度计算*开始----------')
horn_da_d = horn_d - horn_cd_d
horn_da_a = horn_a - horn_ab_a
horn_da_x = 180 - horn_da_d - horn_da_a

print('边DA的长度为:', da)
print('边DX的长度为:', dx)
print('边AX的长度为:', ax)
print('horn_da_d角的度数为:', horn_da_d, '度')
print('horn_da_a角的度数为:', horn_da_a, '度')
print('horn_da_x角的度数为:', horn_da_x, '度')

print('----------horn_da内角角度及各边长度计算*结束----------')
