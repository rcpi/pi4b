import threading
import time
g_num = 200


def test1():
    global g_num
    for i in range(5):
        g_num += 1
    print("--test1,g_num=%d--" % g_num)


def test2():
    global g_num
    print("--test2,g_num=%d--" % g_num)


t1 = threading.Thread(target=test1)
t1.start()
time.sleep(1)
t2 = threading.Thread(target=test2)
t2.start()
