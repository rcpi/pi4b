import time
import RPi.GPIO as GPIO
stepApul = 20  # 频率
stepAdir = 21  # 方向
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(stepAdir, GPIO.OUT)
GPIO.setup(stepApul, GPIO.OUT)
a = 0
while True:
    while a < 10:
        GPIO.output(stepAdir, GPIO.HIGH)
        GPIO.output(stepApul, GPIO.HIGH)
        time.sleep(0.005)
        GPIO.output(stepApul, GPIO.LOW)
        time.sleep(0.005)
        a = a + 1
    while a == 10:
        GPIO.output(stepAdir, GPIO.LOW)
        GPIO.output(stepApul, GPIO.HIGH)
        time.sleep(0.005)
        GPIO.output(stepApul, GPIO.LOW)
        time.sleep(0.005)
        a = a-1
GPIO.cleanup()
